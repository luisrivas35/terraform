FROM ubuntu:22.04
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl wget unzip
# Descarga el binario de Terraform y colócalo en el directorio /usr/local/bin
RUN wget https://releases.hashicorp.com/terraform/0.14.10/terraform_0.14.10_linux_amd64.zip && \
    unzip terraform_0.14.10_linux_amd64.zip && \
    mv terraform /usr/local/bin && \
    rm terraform_0.14.10_linux_amd64.zip

# Establece el directorio de trabajo
WORKDIR /app

# Ejecuta el contenedor en modo interactivo

COPY . .
RUN terraform init
RUN terraform plan -out=tfplan
ENTRYPOINT [ "" ]
CMD ["/bin/bash"]